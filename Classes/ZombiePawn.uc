class ZombiePawn extends Pawn placeable;

/** The pawn's light environment */
var DynamicLightEnvironmentComponent LightEnvironment;
var bool bDead;
var repnotify int ZombieLevel; //0 == basic, the more level, the tougher the zombie
var int biteDamage; // the damage it does when attacking a target


replication
{
	if(bNetDirty)
		ZombieLevel;
}

simulated event ReplicatedEvent(name VarName)
{
	if(VarName == 'ZombieLevel')
	{
		initfunc();
	}
	super.ReplicatedEvent(VarName);
}

simulated event PostBeginPlay()
{
	
	SetTimer(1.0,,'initfunc');
	
	
}

reliable server function initfunc()
{
	Controller = Spawn(ControllerClass);
	Controller.Possess(self,false);
	SetMovementPhysics();

	health = 100 + ZombieLevel*30; // 30 health more per level

	GroundSpeed = 150 + ZombieLevel*20; // 20 uu/seg more per level have fun with this

	biteDamage = 30+ ZombieLevel *10;
}

simulated event  bool Died(controller Killer,Class<DamageType> DamageType,Vector HitLocation)
{
	
	//we add the kill to the counter
	if(ZombieDeathmatch(WorldInfo.Game) != none)
	{
		ZombieDeathmatch(WorldInfo.Game).AddZombieKill();
	}
	return super.Died(Killer,DamageType,HitLocation);
}

simulated function SetDyingPhysics()
{
	if(bDead ==false){
		bDead =true;
	if (Physics == PHYS_RigidBody)
		{
			//@note: Falling instead of None so Velocity/Acceleration don't get cleared
			setPhysics(PHYS_Falling);
		}

		Mesh.SetRBChannel(RBCC_Pawn);
		Mesh.SetRBCollidesWithChannel(RBCC_Default,TRUE);
		Mesh.SetRBCollidesWithChannel(RBCC_Pawn,TRUE);
		Mesh.SetRBCollidesWithChannel(RBCC_Vehicle,TRUE);
		Mesh.SetRBCollidesWithChannel(RBCC_Untitled3,FALSE);
		Mesh.SetRBCollidesWithChannel(RBCC_BlockingVolume,TRUE);
		// Ensure we are always updating kinematic
		Mesh.MinDistFactorForKinematicUpdate = 0.0;

		
		Mesh.ForceSkelUpdate();

		// Move into post so that we are hitting physics from last frame, rather than animated from this
		Mesh.SetTickGroup(TG_PostAsyncWork);

		

		PreRagdollCollisionComponent = CollisionComponent;
		CollisionComponent = Mesh;

		// Turn collision on for skelmeshcomp and off for cylinder
		CylinderComponent.SetActorCollision(false, false);
		Mesh.SetActorCollision(true, true);
		Mesh.SetTraceBlocking(true, true);

		SetPhysics(PHYS_RigidBody);
		Mesh.PhysicsWeight = 1.0;

		// If we had stopped updating kinematic bodies on this character due to distance from camera, force an update of bones now.
		if( Mesh.bNotUpdatingKinematicDueToDistance )
		{
			Mesh.UpdateRBBonesFromSpaceBases(TRUE, TRUE);
		}

		Mesh.PhysicsAssetInstance.SetAllBodiesFixed(FALSE);
		Mesh.bUpdateKinematicBonesFromAnimation=FALSE;

		// Set all kinematic bodies to the current root velocity, since they may not have been updated during normal animation
		// and therefore have zero derived velocity (this happens in 1st person camera mode).
		Mesh.SetRBLinearVelocity(Velocity, false);

		
		// reset mesh translation since adjustment code isn't executed on the server
		// but the ragdoll code uses the translation so we need them to match up for the
		// most accurate simulation
		
		// we'll use the rigid body collision to check for falling damage
		
		Mesh.SetNotifyRigidBodyCollision(true);
		Mesh.WakeRigidBody();
	}
}
DefaultProperties
{

	Components.Remove(Sprite)

	Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
		bSynthesizeSHLight=TRUE
		bIsCharacterLightEnvironment=TRUE
		bUseBooleanEnvironmentShadowing=FALSE
		InvisibleUpdateTime=1
		MinTimeBetweenFullUpdates=.2
	End Object
	Components.Add(MyLightEnvironment)

	LightEnvironment=MyLightEnvironment

bDead =false;
	Begin Object  class=SkeletalMeshComponent Name=WPawnSkeletalMeshComponent
		skeletalmesh=SkeletalMesh'CH_IronGuard_Male.Mesh.SK_CH_IronGuard_MaleA'
		PhysicsAsset=PhysicsAsset'CH_AnimCorrupt.Mesh.SK_CH_Corrupt_Male_Physics'
		bCacheAnimSequenceNodes=FALSE
		AlwaysLoadOnClient=true
		AlwaysLoadOnServer=true
		bOwnerNoSee=true
		CastShadow=true
		BlockRigidBody=TRUE
		bUpdateSkelWhenNotRendered=false
		bIgnoreControllersWhenNotRendered=TRUE
		bUpdateKinematicBonesFromAnimation=true
		bCastDynamicShadow=true
		Translation=(Z=8.0)
		RBChannel=RBCC_Untitled3
		RBCollideWithChannels=(Untitled3=true)
		LightEnvironment=MyLightEnvironment
		bOverrideAttachmentOwnerVisibility=true
		bAcceptsDynamicDecals=FALSE
		AnimTreeTemplate=AnimTree'CH_AnimHuman_Tree.AT_CH_Human'
		AnimSets[0] = AnimSet'CH_AnimHuman.Anims.K_AnimHuman_BaseMale'


		bHasPhysicsAssetInstance=true
		TickGroup=TG_PreAsyncWork
		MinDistFactorForKinematicUpdate=0.2
		bChartDistanceFactor=true
		//bSkipAllUpdateWhenPhysicsAsleep=TRUE
		RBDominanceGroup=20
		Scale=1.075
		// Nice lighting for hair
		bUseOnePassLightingOnTranslucency=TRUE
		bPerBoneMotionBlur=true
	End Object
	Mesh=WPawnSkeletalMeshComponent
	Components.Add(WPawnSkeletalMeshComponent)


	Begin Object Name=CollisionCylinder
		CollisionRadius=+0021.000000
		CollisionHeight=+0044.000000
	End Object

	CylinderComponent=CollisionCylinder
	ControllerClass = class'ZombieAIController'

	GroundSpeed=150;
}
