class ZombieSpawnPoint extends actor implements (ISpawnPoint) placeable;

reliable server function SpawnZombie(class<ZombiePawn> ZombieType,int ZombieLevel)
{
	local ZombiePawn spawned;
	Spawned =Spawn(ZombieType,,,Location,Rotation,,true);
	SPawned.ZombieLevel=ZombieLevel;
}


DefaultProperties
{
	Begin Object Class=SpriteComponent Name=Sprite
		Sprite=Texture2D'EditorResources.S_Note'
		HiddenGame=True
		AlwaysLoadOnClient=False
		AlwaysLoadOnServer=False
		SpriteCategoryName="Notes"
	End Object
	Components.Add(Sprite)
}
