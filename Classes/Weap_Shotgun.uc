class Weap_Shotgun extends ReloadableWeapon;

var int numShells; // this will be how many shoots it does at once

replication
	{
		if(bNetDirty)
			numShells;
	}

simulated function rotator AddSpread(rotator BaseAim)
{
	
	local vector X, Y, Z;
	local float CurrentSpread, RandY, RandZ;
	`log("AddSpread" @ self);

	CurrentSpread = Spread[CurrentFireMode];
	`log("CurrentSpread is" @ CurrentSpread);
	if (CurrentSpread == 0)
	{
		return BaseAim;
	}
	else
	{
		// Add in any spread.
		GetAxes(BaseAim, X, Y, Z);
		RandY = FRand() - 0.5;
		RandZ = Sqrt(0.5 - Square(RandY)) * (FRand() - 0.5);
		return rotator(X + RandY * CurrentSpread * Y + RandZ * CurrentSpread * Z);
	}
}

simulated function FireAmmunition()
{
	// Use ammunition to fire
	ConsumeAmmo( CurrentFireMode );

	// Handle the different fire types
	switch( WeaponFireTypes[CurrentFireMode] )
	{
		case EWFT_InstantHit:
			`log("EWFT_INSTAHIT");
			InstantFire();
			break;

		case EWFT_Projectile:
			ProjectileFire();
			break;

		case EWFT_Custom:
			CustomFire();
			break;
	}

	NotifyWeaponFired( CurrentFireMode );
}


simulated function InstantFire()
{
	
	

	local int n;
	

	n = numShells;
	for(n = numShells; n >0; n--)
	{
		super.InstantFire();
	}
}




DefaultProperties
{
	InstantHitDamage(0)=40
	FireInterval(0)=+2
	numShells = 6;
	WeaponFireTypes(0)=EWFT_InstantHit


	AmmoCount=20
	LockerAmmoCount=20
	MaxAmmoCount=40

	IconCoordinates=(U=728,V=382,UL=162,VL=45)

	WeaponColor=(R=160,G=0,B=255,A=255)

	InventoryGroup=4
	GroupWeight=0.5

	IconX=400
	IconY=129
	IconWidth=22
	IconHeight=48
	
	Spread[0] = 0.25;

	Begin Object Name=FirstPersonMesh
		SkeletalMesh=SkeletalMesh'WP_ShockRifle.Mesh.SK_WP_ShockRifle_1P'
		AnimSets(0)=AnimSet'WP_ShockRifle.Anim.K_WP_ShockRifle_1P_Base'
	
		Rotation=(Yaw=-16384)
		FOV=60.0
	End Object

	AttachmentClass=class'UTGameContent.UTAttachment_ShockRifle'

	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WP_ShockRifle.Mesh.SK_WP_ShockRifle_3P'
	End Object
}
