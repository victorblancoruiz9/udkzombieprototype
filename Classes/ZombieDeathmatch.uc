class ZombieDeathmatch extends ZombieGameInfo;

var int Score;
var int Kills;

var int CurrentRound;

var Array< ISpawnPoint> ZombieSpawns;
var int NumSpawns;
var int ZombiesLeft;
var int ZombiesToSpawn;



reliable server function AddZombieKill()
{
	Kills++;
	Score += 100;
	ZombiesLeft -=1;
	broadcast(self,"your Score is" @ Score);
	broadcast(self,"ZombiesLeft = " @ Zombiesleft);

	if(ZombiesLeft <= 1)
	{
		
		//we cancel the spawner
		ClearTimer('SpawnNewZombie');
		broadcast(self,'RoundClear');

		StartNewRound(CurrentRound +1);
	}


}

reliable server function StartNewRound(int RoundNumber)
{
	
	ZombiesLeft =  5+RoundNumber*3;
	ZombiesToSpawn = ZombiesLeft;
	broadcast(self,"StartRound" @ roundNumber);
	broadcast(self,"ZombiesLeft = " @ Zombiesleft);

	if(RoundNumber == 0)
	{
		AddSpawnPoints();
		CurrentRound=0;
	}
	CurrentRound=RoundNumber;

	SetTimer(1,true,'SpawnNewZombie');
}



reliable server function SpawnNewZombie()
{
	local ISpawnPoint  SP;
	local bool foundSpawn;
	local int I;
	foundSpawn = false;
	
	ZombiesToSpawn -=1;
	if(ZombiesToSpawn <= 0)
	{
		
		//we cancel the spawner
		broadcast(self,'TimerClear');
		ClearTimer('SpawnNewZombie');
		return;

		
	}

	While( !foundSpawn)
	{
		//SearchRandom Spawn
		I = Rand(NumSpawns);

		SP = ZombieSpawns[I];
		
		SP.SpawnZombie(Class'ZombiePawn',CurrentRound) ; // we set the current round as the zombie level, now they are stronger each round
		foundSpawn = true;

	}


}

reliable server function AddSpawnPoints()
{
	local actor  SP;
	ForEach WorldInfo.AllActors(class'actor',SP,class'ISpawnPoint')
	{
		ZombieSpawns.AddItem(ISpawnPoint(SP));
	}
	NumSpawns=ZombieSpawns.Length;
	broadcast(self, "NumSpawnPoints = " @ ZombieSpawns.Length);
}


exec function StartRounds()
{
	StartNewRound(0);
}
simulated event PostBeginPlay()
{
	
	StartNewRound(0);
	super.PostBeginPlay();
}

function EndGame(PlayerReplicationInfo Winner, string Reason )
{
	//we want infinite game, so empty for now
}

DefaultProperties
{
	
}
