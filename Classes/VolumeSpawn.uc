class VolumeSpawn extends Volume implements (ISpawnPoint);

var Box BoundingBox;


reliable server function SpawnZombie(class<ZombiePawn> ZombieType,int ZombieLevel)
{
	local ZombiePawn Spawned;
	 while (spawned == none)
      {
            spawned = Spawn(ZombieType, ,,RandomLocation());
      }
		
	Spawned.ZombieLevel=ZombieLevel;


}


simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	GetComponentsBoundingBox(BoundingBox);
}


simulated function vector RandomLocation()
{
	local vector result;

	result = BoundingBox.Min;
	result.X += FRand() * (BoundingBox.Max.X - BoundingBox.Min.X);
	result.Y += FRand() * (BoundingBox.Max.Y - BoundingBox.Min.Y);

	return result;
}

DefaultProperties
{
}
