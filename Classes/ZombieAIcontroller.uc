class ZombieAIController extends UDKBot;

var Pawn Target;
var() Vector TempDest;

replication
{
	if(bNetDirty)
		Target,TempDest;
}


simulated event SeePlayer(pawn seen)
{
	if(Target == none)
	{
	Target = seen;
	//WorldInfo.Game.Broadcast(self, "SeeYa");
	gotoState('Pursue');
	}
}

simulated event Possess(Pawn inPawn,bool bVehicleTransition)
{
	//WorldInfo.Game.Broadcast(self, "possessedAI");
	
	super.Possess(inPawn,bVehicleTransition);

	//this is just a quick fix, TODO: implement wandering
	target  = GetALocalPlayerController().pawn;
	if (Target != none)
	{
		gotoState('Pursue');
	}
	else
	{   gotoState('Idle');
	}
}

 auto simulated state Idle
{
	event BeginState(name PreviousStateName)
	{
		Target =none;

		
	}
Begin:

	
	Sleep(1);
goto('Begin');
}


simulated event Bumped(Actor Other)
{
	if(Pawn(Other) == Target)
	{
		SetTimer(0.5,,'DoDamage');
	}
}
simulated function CheckDistance()
{
	if(VSize2D(Target.Location - Pawn.Location) <= 100)
	{
		DoDamage();
	}
}

simulated function DoDamage()
{
	local vector Hit;
	local vector Momentum;
	Target.TakeDamage(ZombiePawn(Pawn).biteDamage,self,Hit,Momentum,class'DmgType_Crushed');
}

/**
 * This will try several methods to reach DestTarget:
 * Returns 1 if target is directly reachable, no tricky navigation required
 * Returns 2 if path can be found using navmesh. 
 * Returns 3 if path can be found using pathnodes. 
 * Returns 0 if no path to the target can be found. 
 */
simulated function int FindBestPath(Actor DestTarget)
{
	// Check if the target is directly reachable
	if (NavigationHandle.ActorReachable(DestTarget) || ActorReachable(DestTarget))
	{
		return 1;
	}
	// Failing that, check if a path can be found using navmesh
	else if(FindNavMeshPath(DestTarget))
	{
		return 2;
	}
	// Failing that, check if a path can be found using pathnodes
	else if (FindPathToward(DestTarget) != none)
	{
		return 3;
	}
	// Otherwise return a fail code
	return 0;
}

simulated function bool FindNavMeshPath(Actor objetive)
{
        // Clear cache and constraints (ignore recycling for the moment)
        NavigationHandle.PathConstraintList = none;
        NavigationHandle.PathGoalList = none;

        // Create constraints
        class'NavMeshPath_Toward'.static.TowardGoal(NavigationHandle, objetive);
        class'NavMeshGoal_At'.static.AtActor(NavigationHandle, objetive, 32);

        // Find path
        return NavigationHandle.FindPath();
}

simulated state Pursue
{
	ignores SeePlayer;
	event BeginState(name PreviousStateName)
	{
	
		setTimer(0.3,true,'CheckDistance');
	}
	event EndState(name NextStateName)
	{
		ClearAllTimers();
	}


Begin:
	switch(FindBestPath(Target))
		{
			// If the enemy is directly reachable then run straight for them
		case 1:
			MoveToward(Target, Target, 30); break;
		case 2:
			// If not, but I can find a path with navmesh then use that
			NavigationHandle.SetFinalDestination(Target.Location);
			if (NavigationHandle.GetNextMoveLocation(TempDest, Pawn.GetCollisionRadius()))
			{
				MoveTo(TempDest, target);
			}
			break;
		case 3:
			// If not, but I can find a path using pathnodes then use that instead
            MoveToward(FindPathToward(target), target);
			break;
		Default:
                                          // I've tried everything and I can't reach my target, give up and go idle
			Target = none;
			GoToState('Idle');
		}
 
    goto 'Begin';
}


DefaultProperties
{
	
}
