class ReloadableWeapon extends UTWeapon;

var int MagazineSize;
var int BulletsPerReload; // for shotgun, so it reloads 1 by 1
var float ReloadTime; // time that each reload takes
var int LoadedBullets;

var  bool bIsReloading;

replication
{
	if(bNetDirty)
		bIsReloading,ReloadTime,MagazineSize,BulletsPerReload;
}


simulated function Reload()
{
	`log("Reload" @ BulletsPerReload @ "bullets");
	LoadedBullets += BulletsPerReload;
	if(MagazineSize <= LoadedBullets)
	{
		ClearTimer('Reload');
		gotoState('active');
	}
}

simulated function StartFire(byte FireModeNum)
{
	
	if(FireModeNum == 0) // leftclick
	{
		if(LoadedBullets <=0) // no bullets in chamber
		{
			`log("NoAmmo in chamber");
			StartReload();
		}
		else
		{
			LoadedBullets--;
			Super.StartFire(FireModeNum);
		}
	}
}

simulated function StartReload()
{
	`log("Reloading");
	gotoState('Reloading');
}
simulated state Reloading
{
	simulated event BeginState(Name PreviousStateName)
	{
		`log("StateReload");
		SetTimer(ReloadTime,true,'Reload');
	}
	simulated event EndState(Name NextStateName)
	{
		ClearTimer('Reload');
	}

}


DefaultProperties
{
	MagazineSize=5;
	BulletsPerReload=5;

	ReloadTime=1;
}
